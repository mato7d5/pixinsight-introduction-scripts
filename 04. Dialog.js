/* PURPOSE
   Construct an empty custom dialog box.

   LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*
 * Default pattern to instantiate a custom dialog box.
 */
function LchSaturationDialog() {
   // -----------------------------------------------------------------------------------------
   // Adopted pattern to inject all Dialog properties into LchSaturationDialog
   this.__base__ = Dialog;
   this.__base__();
   // -----------------------------------------------------------------------------------------

   // here we can start instantiating controls and adding them to the dialog...
}
// prototype chain must be correctly set to access the Dialog parent's methods
LchSaturationDialog.prototype = new Dialog;

 /*
  * This function shows the custom dialog box, interrupts the control flow and
  * waiting the dialog box to be dismissed.
  */
function showDialog() {
   // instantiate the dialog object
   let diag = new LchSaturationDialog;
   // show the dialog box
   return diag.execute();
}

function main() {
   // NOTE: We don't need to show the console since when dialog is shown the
   //       console is disclosed by default.

   // show the dialog and retrieve the returned value on dismissal
   let retVal = showDialog();
   Console.noteln("Dialog dismissed with value: ", retVal);
}

main();
