/* PURPOSE
   Execute your first script and print messages on the console.

   NOTES
   This script prints some messages on the console using four different methods
   provided by the Console object. You should pick the right print function
   depending on the severity of the message:
   - writeln is used for simple log messages
   - noteln highlights a text the log message giving a higher visual importance
   - warningln shows a warning message, tipically reporting issues that don't
               block the script execution
   - criticalln shows a critical message, tipically the script gets interrupted
                after this message

  LICENSE
   This script is part of the "An Introduction to PixInsight PJSR Scripting" workshop.

   Copyright (C) 2020 Roberto Sartori.

   This program is free software: you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation, version 3 of the License.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Show the console
Console.show();

// print to console
Console.writeln("writeln: Hello World");
Console.noteln("noteln: Hello World");
Console.warningln("warningln: Hello World");
Console.criticalln("criticalln: Hello World");
